//
// Created by Хумай Байрамова on 24.12.2021.
//

#include "input_output.h"
#include "error_handler.h"

enum open_status open_file(const char *filename, FILE **file, const char *mode) {

    if (!filename) {
        return OPEN_INVALID_FILENAME;
    }

    if (!mode) {
        return OPEN_INVALID_MODE;
    }
    *file = fopen(filename, mode);

    if (file == NULL) {
        return OPEN_CANT_OPEN;
    }

    return OPEN_OK;
}

enum close_status close_file(FILE *file) {

    if (fclose(file) == EOF) {
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}




