//
// Created by Хумай Байрамова on 26.12.2021.
//

#include "rotation.h"

enum rotate_status rotate(const struct image *src, struct image *res) {
    res->width = src->height;
    res->height = src->width;
    if (src->data == NULL) {
        res->data = NULL;
        return ROTATE_DATA_IS_NULL;
    };


    struct pixel *pixels = malloc(sizeof(struct pixel) * src->width * src->height);

    if (!pixels) {
        res->data = NULL;
        return ROTATE_CANT_ALLOCATE;
    }

    for (size_t i = 0; i < src->height; i++) {
        for (size_t j = 0; j < src->width; j++) {
            pixels[src->height * j + (src->height- 1 - i) ] = src->data [i * src->width + j];
        }
    }

    res->data = pixels;

    return ROTATE_OK;
}

