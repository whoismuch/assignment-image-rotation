//
// Created by Хумай Байрамова on 26.12.2021.
//

#include "image.h"

struct image create_image(const size_t width, const size_t height) {

    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);

    if (!pixels) {
        return (struct image) {
                .width = width,
                .height = height,
                .data = NULL
        };
    }

    return (struct image) {
            .width = width,
            .height = height,
            .data = pixels
    };
}

void free_image(struct image *image) {
    free(image->data);
}

