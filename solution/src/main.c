#include "bmp.h"
#include "input_output.h"
#include "rotation.h"
#include <stdio.h>


int main(int argc, char **argv) {
    (void) argc;
    (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        fprintf(stderr, "Формат работы с программой следующий: ./image-transformer <source-image> <transformed-image>");
        return 1;
    }

    FILE *in_file;
    FILE *out_file;

    struct status stat = {0};

    stat.type = OPEN_STATUS;
    stat.openStatus = open_file(argv[1], &in_file, FILE_RB);
    if (!check_stat(stat)) return 1;

    struct image img = {0};

    stat.type = READ_STATUS;
    stat.readStatus = from_bmp(in_file, &img);
    if (!check_stat(stat)){
        close_file(in_file);
        return 1;
    }

    stat.type = CLOSE_STATUS;
    stat.closeStatus = close_file(in_file);
    if (!check_stat(stat)) return 1;

    struct image resultImg = {0};

    stat.type = ROTATE_STATUS;
    stat.rotateStatus = rotate(&img, &resultImg);
    if (!check_stat(stat)) {
        close_file(in_file);
        free_image(&img);
        free_image(&resultImg);
        return 1;
    }

    stat.type = OPEN_STATUS;
    stat.openStatus = open_file(argv[2], &out_file, FILE_WB);
    if (!check_stat(stat)) {
        close_file(in_file);
        free_image(&img);
        free_image(&resultImg);
        return 1;
    }

    stat.type = WRITE_STATUS;
    stat.writeStatus = to_bmp(out_file, &resultImg);
    if (!check_stat(stat)){
        close_file(out_file);
        free_image(&img);
        free_image(&resultImg);
        return 1;
    }

    free_image(&img);
    free_image(&resultImg);

    stat.type = CLOSE_STATUS;
    stat.closeStatus = close_file(out_file);
    if (!check_stat(stat)) return 1;

    return 0;

}


