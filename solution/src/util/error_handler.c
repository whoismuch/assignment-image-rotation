//
// Created by Хумай Байрамова on 27.12.2021.
//

#include "error_handler.h"

bool check_stat(struct status status) {
    if (status.type == OPEN_STATUS && status.openStatus) {
        print_message_open(status.openStatus);
        return false;
    }
    if (status.type == CLOSE_STATUS && status.closeStatus) {
        print_message_close(status.closeStatus);
        return false;
    }
    if (status.type == READ_STATUS && status.readStatus) {
        print_message_read(status.readStatus);
        return false;
    }
    if (status.type == WRITE_STATUS && status.writeStatus) {
        print_message_write(status.writeStatus);
        return false;
    }
    if (status.type == ROTATE_STATUS && status.rotateStatus) {
        print_message_rotate(status.rotateStatus);
        return false;
    }
    return true;
}

char *message_write[] = {
        [WRITE_OK] = "Записали успешно",
        [WRITE_FILE_IS_NULL] = "Неплохо было бы передать файлик, которй не NULL",
        [WRITE_INVALID_HEADER] = "Не удалось записать заголовок формата в файл",
        [WRITE_INVALID_PADDING] = "Oops, у нас проблемы с доп отступами",
        [WRITE_INVALID_BITS] = "Записали неверное количество битов"
};

char *message_read[] = {
        [READ_OK] = "Прочли успешно",
        [READ_INVALID_SIGNATURE] = "Сигнатура формата в файле неверна",
        [READ_INVALID_BITS] = "Прочли неверное количество битов",
        [READ_INVALID_HEADER] = "Не удалось прочесть заголовок формата в файл",
        [READ_FILE_IS_NULL] = "Неплохо было бы передать файлик, которй не NULL",
        [READ_INVALID_PADDING] = "Oops, у нас проблемы с доп отступами",
        [READ_CANT_CREATE_IMAGE] = "Не получилось создать изображение"
};

char *message_open[] = {
        [OPEN_OK] = "Открыли успешно",
        [OPEN_INVALID_FILENAME] = "Не можем открыть входной файлик, можно подать не null, а нормальное имя файла?",
        [OPEN_INVALID_MODE] = "Не можем открыть входной файлик, можно в качестве mode подать не null?",
        [OPEN_CANT_OPEN] = "Не можем открыть входной файлик"
};

char *message_close[] = {
        [CLOSE_OK] = "Закрыли успешно",
        [CLOSE_ERROR] = "Возникла ошибка при закрытии файла"
};

char *message_rotate[] = {
        [ROTATE_OK] = "Картинка повернута успешно",
        [ROTATE_CANT_ALLOCATE] = "Не получилось саллоцировать память",
        [ROTATE_DATA_IS_NULL] = "Дата в исходном изображении = NULL"
};


void print_message_read(enum read_status status) {
    fprintf(stderr, "%s", message_read[status]);
}

void print_message_write(enum write_status status) {
    fprintf(stderr, "%s", message_write[status]);
}

void print_message_open(enum open_status status) {
    fprintf(stderr, "%s", message_open[status]);
}

void print_message_close(enum close_status status) {
    fprintf(stderr, "%s", message_close[status]);
}

void print_message_rotate(enum rotate_status status) {
    fprintf(stderr, "%s", message_rotate[status]);
}

