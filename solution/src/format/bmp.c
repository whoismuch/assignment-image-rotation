//
// Created by Хумай Байрамова on 24.12.2021.
//

#include "bmp.h"
#include "malloc.h"
#include "stdio.h"

#define BMP 19778

static size_t calculate_padding(size_t width) {
    return (4 - width * 3 % 4) % 4;
}

static struct bmp_header create_header(uint32_t width, uint32_t height) {
    const uint32_t image_size = width * height * sizeof(struct pixel) + calculate_padding(width) * height;
    return (struct bmp_header) {
            .bfType = BMP,
            .bfileSize = image_size + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum read_status from_bmp(FILE *in, struct image *img) {

    if (!in) return READ_FILE_IS_NULL;
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_HEADER;
    if (header.bfType != BMP) return READ_INVALID_SIGNATURE;
    if (header.biWidth <= 0 || header.biHeight <= 0) return READ_INVALID_BITS;

    *img = create_image(header.biWidth, header.biHeight);

    if (!img->data) return READ_CANT_CREATE_IMAGE;

    size_t const padding = calculate_padding(img->width);

    for (size_t i = 0; i < img->height; i++) {
        size_t byte_read = fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in);
        if (byte_read != img->width) {
            free_image(img);
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR)) {
            free_image(img);
            return READ_INVALID_PADDING;
        }
    }

    return READ_OK;

}

enum write_status to_bmp(FILE *out, struct image const *img) {

    if (!out) return WRITE_FILE_IS_NULL;
    struct bmp_header header = create_header(img->width, img->height);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) return WRITE_INVALID_HEADER;
    size_t const padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        size_t byte_wrote = fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out);
        if (byte_wrote != img->width) return WRITE_INVALID_BITS;
        if (fseek(out, padding, SEEK_CUR))
            return WRITE_INVALID_PADDING;
    }
    return WRITE_OK;
}

