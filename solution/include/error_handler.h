//
// Created by Хумай Байрамова on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ERROR_HANDLER_H
#define ASSIGNMENT_IMAGE_ROTATION_ERROR_HANDLER_H

#include <stdio.h>
#include <stdbool.h>
#include "malloc.h"

enum status_type {
    READ_STATUS,
    WRITE_STATUS,
    OPEN_STATUS,
    CLOSE_STATUS,
    ROTATE_STATUS
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_IS_NULL,
    READ_INVALID_PADDING,
    READ_CANT_CREATE_IMAGE
};

enum write_status {
    WRITE_OK = 0,
    WRITE_FILE_IS_NULL,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_PADDING,
    WRITE_INVALID_BITS
};

enum open_status {
    OPEN_OK = 0,
    OPEN_INVALID_FILENAME,
    OPEN_INVALID_MODE,
    OPEN_CANT_OPEN
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR,
};

enum rotate_status {
    ROTATE_OK = 0,
    ROTATE_DATA_IS_NULL,
    ROTATE_CANT_ALLOCATE,
};

struct status {
    enum status_type type;
    union {
        enum read_status readStatus;
        enum write_status writeStatus;
        enum open_status openStatus;
        enum close_status closeStatus;
        enum rotate_status rotateStatus;
    };
};

void print_message_read(enum read_status const status);

void print_message_write(enum write_status const status);

void print_message_open(enum open_status const status);

void print_message_close(enum close_status const status);

void print_message_rotate(enum rotate_status const status);

bool check_stat(struct status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_ERROR_HANDLER_H
