//
// Created by Хумай Байрамова on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATION_H

#include "image.h"
#include "error_handler.h"

enum rotate_status rotate(const struct image* src, struct image* res);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
