//
// Created by Хумай Байрамова on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};

struct image create_image(const size_t width, const size_t height);

void free_image(struct image *image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
