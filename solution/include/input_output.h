//
// Created by Хумай Байрамова on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_INPUT_OUTPUT_H
#define ASSIGNMENT_IMAGE_ROTATION_INPUT_OUTPUT_H

#include "error_handler.h"
#include <stdbool.h>
#include <stdio.h>

#define FILE_RB "rb"
#define FILE_WB "wb"

enum open_status open_file(const char* filename, FILE** file, const char* mode);

enum close_status close_file(FILE* file);

#endif //ASSIGNMENT_IMAGE_ROTATION_INPUT_OUTPUT_H
